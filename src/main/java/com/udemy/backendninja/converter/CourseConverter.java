package com.udemy.backendninja.converter;

import org.springframework.stereotype.Component;
import com.udemy.backendninja.entity.Course;
import com.udemy.backendninja.model.CourseModel;




@Component("courseConverter")
public class CourseConverter {
	
	// Primero transformar de una  Entity a un model
	public CourseModel entity2model(Course course) {
		CourseModel courseModel = new CourseModel();
		courseModel.setName(course.getName());
		courseModel.setDescription(course.getDescription());
		courseModel.setPrice(course.getPrice());
		courseModel.setHours(course.getHours());
		return courseModel;
	}
	
	
	// Segundo transformar de un Model a una Entity
	// hacemos la inversa
	public Course model2entity (CourseModel courseModel) { // tenemos que devolver un Entity
		Course  course = new Course();
		course.setName(courseModel.getName());
		course.setDescription(courseModel.getDescription());
		course.setPrice(courseModel.getPrice());
		course.setHours(courseModel.getHours());
		
		return course;
		
		
	}
}
