package com.udemy.backendninja.controller;

import org.apache.commons.logging.Log; //preguntar si es esta clase de LOG
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.udemy.backendninja.entity.Course;
import com.udemy.backendninja.service.CourseService;

@Controller
@RequestMapping("/courses")
public class CourseController {
	
	private static final String COURSES_VIEW = "course";
	
	private static final Log LOG = LogFactory.getLog(CourseController.class); //fijarse bien aca! ...esta bien aca el log..!!!
	
	@Autowired
	@Qualifier("courseServiceImpl")
	private CourseService courseService;
	
//	listAllcourse
	@GetMapping("/listcourses")
	public ModelAndView listAllCourse() {
		LOG.info("Call: " + "listAllCourse()"); // LOG.info llama a este metodo
		ModelAndView mav = new ModelAndView(COURSES_VIEW);
		mav.addObject("course", new Course()); //creamos un objeto vacio para que thyleaf pueda trabajar en el formulario
		mav.addObject("courses", courseService.listAllCourse());
		return mav;
		
	}
//	Add course
	@PostMapping("/addcourse")
	public String addCourse(@ModelAttribute("course") Course course) {
		LOG.info("Call: " + "addCourse()" + "--Param: " + course.toString()); //
		courseService.addCourse(course);
		return "redirect:/courses/listcourses";
		
		//localhost:8080/courses/listcourses
		
	}
}
